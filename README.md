# Home Router via Ansible

This is an ansible playbook that makes use of multiple roles to configure a multi-interfaced machine to be a router. This system has only been tested with Ansible 2+ and CentOS 7.

## Roles

### Router (tag = 'router')

Handles configuration of router-things. It sets up the firewall for NAT as well as DNS and DHCP.

### QoS (tag = 'qos') 

Handles Quality of Service configuration using FireQoS

### Statistics and Monitoring (tag = 'stats' or 'netdata')

## Files

Most of the important files are in ./roles/ and specify what each role does. The root playbook is the `home-site.yml` file.

## Usage

* Modify the template at `./roles/home-router/vars/main.yml.template` to match your desired setup and save as `./roles/home-router/vars/main.yml`
* Execute the playbook with `ansible-playbook ./home-site.yml -i hosts`

